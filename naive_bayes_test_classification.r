rm(list = ls())

#Another way to set as working directory
#setwd("C:/dorAvital/MachineLearning/R_project") #Change it to the location of the dataaset in your local machine

#Laoding packages-----------------------------------------------------------------------------------------
library(e1071)
install.packages("mlr")
library(mlr)

#Loading data---------------------------------------------------------------------------------------------
train <- read.csv("train.csv")
test <- read.csv("test.csv")

#Taking Sample from training data as test data for accuracy purpose---------------------------------------
pseudo_test <- train[c(1:70000),]
pseudo_train <- train[c(70001:nrow(train)),]

#Create a classification task for learning and specify the target feature---------------------------------
task1 = makeClassifTask(data = pseudo_train, target = "toxic")
task2 = makeClassifTask(data = pseudo_train, target = "severe_toxic")
task3 = makeClassifTask(data = pseudo_train, target = "obscene")
task4 = makeClassifTask(data = pseudo_train, target = "threat")
task5 = makeClassifTask(data = pseudo_train, target = "insult")
task6 = makeClassifTask(data = pseudo_train, target = "identity_hate")

#Initialize the Naive Bayes classifier--------------------------------------------------------------------
selected_model = makeLearner("classif.naiveBayes")

#Train the model------------------------------------------------------------------------------------------
NB_mlr1 = train(selected_model, task1)
NB_mlr2 = train(selected_model, task2)
NB_mlr3 = train(selected_model, task3)
NB_mlr4 = train(selected_model, task4)
NB_mlr5 = train(selected_model, task5)
NB_mlr6 = train(selected_model, task6)

#Read the model learned-----------------------------------------------------------------------------------  
#NB_mlr1$learner.model

#Predict on the dataset without passing the target feature------------------------------------------------
predict_toxic = as.data.frame(predict(NB_mlr1, newdata =as.data.frame(pseudo_test[,2])))
predictt_severe_toxic = as.data.frame(predict(NB_mlr2, newdata =as.data.frame(pseudo_test[,2])))
predict_obscene = as.data.frame(predict(NB_mlr3, newdata =as.data.frame(pseudo_test[,2])))
predict_threat = as.data.frame(predict(NB_mlr4, newdata =as.data.frame(pseudo_test[,2])))
predict_insult = as.data.frame(predict(NB_mlr5, newdata =as.data.frame(pseudo_test[,2])))
predict_identity = as.data.frame(predict(NB_mlr6, newdata =as.data.frame(pseudo_test[,2])))

pseudo_test <- cbind(pseudo_test,predict_toxic)
pseudo_test <- cbind(pseudo_test,predictt_severe_toxic)
pseudo_test <- cbind(pseudo_test,predict_obscene)
pseudo_test <- cbind(pseudo_test,predict_threat)
pseudo_test <- cbind(pseudo_test,predict_insult)
pseudo_test <- cbind(pseudo_test , predict_identity)

colnames(pseudo_test) <- c("id", "comment", "actual_toxic", "actual severe toxic" , "actual obscene" ,
                           "actual threat", "actual insult" , "actual identity" , "predicted toxic",
                           "predicted severe toxic" , "predicted obscene", "predicted threat" ,
                           "predicted insult", "predicted identity")

#Calculating Accuracy-------------------------------------------------------------------------------------
tox1 <- table(pseudo_test$actual_toxic ,pseudo_test$`predicted toxic`)
sum(diag(tox1))/sum(tox1)
tox2 <- table(pseudo_test$`actual severe toxic` ,pseudo_test$`predicted severe toxic`)
sum(diag(tox2))/sum(tox2)
tox3 <- table(pseudo_test$`actual obscene` ,pseudo_test$`predicted obscene`)
sum(diag(tox3))/sum(tox3)
tox4 <- table(pseudo_test$`actual threat` ,pseudo_test$`predicted threat`)
sum(diag(tox4))/sum(tox4)
tox1 <- table(pseudo_test$`actual insult` ,pseudo_test$`predicted insult`)
sum(diag(tox1))/sum(tox1)


#Applying Same technique on actual test data-------------------------------------------------------------

#Create a classification task for learning and specify the target feature---------------------------------
task1 = makeClassifTask(data = train, target = "toxic")
task2 = makeClassifTask(data = train, target = "severe_toxic")
task3 = makeClassifTask(data = train, target = "obscene")
task4 = makeClassifTask(data = train, target = "threat")
task5 = makeClassifTask(data = train, target = "insult")
task6 = makeClassifTask(data = train, target = "identity_hate")

#Initialize the Naive Bayes classifier--------------------------------------------------------------------
selected_model = makeLearner("classif.naiveBayes")

#Train the model------------------------------------------------------------------------------------------
NB_mlr1 = train(selected_model, task1)
NB_mlr2 = train(selected_model, task2)
NB_mlr3 = train(selected_model, task3)
NB_mlr4 = train(selected_model, task4)
NB_mlr5 = train(selected_model, task5)
NB_mlr6 = train(selected_model, task6)

#Read the model learned-----------------------------------------------------------------------------------  
#NB_mlr1$learner.model

#Predict on the dataset without passing the target feature------------------------------------------------
predict_toxic = as.data.frame(predict(NB_mlr1, newdata =as.data.frame(test[,2])))
predict_severe_toxic = as.data.frame(predict(NB_mlr2, newdata =as.data.frame(test[,2])))
predict_obscene = as.data.frame(predict(NB_mlr3, newdata =as.data.frame(test[,2])))
predict_threat = as.data.frame(predict(NB_mlr4, newdata =as.data.frame(test[,2])))
predict_insult = as.data.frame(predict(NB_mlr5, newdata =as.data.frame(test[,2])))
predict_identity = as.data.frame(predict(NB_mlr6, newdata =as.data.frame(test[,2])))

test <- cbind(test,predict_toxic)
test <- cbind(test,predict_severe_toxic)
test <- cbind(test,predict_obscene)
test <- cbind(test,predict_threat)
test <- cbind(test,predict_insult)
test <- cbind(test , predict_identity)

colnames(test) <- c("id", "comment", "predicted toxic",
                           "predicted severe toxic" , "predicted obscene", "predicted threat" ,
                           "predicted insult", "predicted identity")

write.csv(test,"submission_naive_bayes.csv")















