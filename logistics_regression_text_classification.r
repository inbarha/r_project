rm(list = ls())

#Another way to set as working directory
#setwd("C:/dorAvital/MachineLearning/R_project") #Change it to the location of the dataaset in your local machine

install.packages("text2vec")
install.packages("tidyr")
install.packages("stringr")
install.packages("Matrix")
install.packages("SparseM")
install.packages("LiblineaR")
install.packages("tm")
install.packages("stringi")
library(text2vec)
library(dplyr)
library(tidyr)
library(stringr)
library(Matrix)
library(SparseM)
library(LiblineaR)
library(NLP)
library(tm)
library(stringi)
# Load data --------------------------------------------------------------------------------------------
train <- read.csv('train.csv',stringsAsFactors = FALSE)
test <- read.csv('test.csv',stringsAsFactors = FALSE)
# Replace NA values -----------------------------------------------------------------------------------
train <- train %>% replace_na(list(comment_text = "unknown")) %>% tbl_df()
test <- test %>% replace_na(list(comment_text = "unknown")) %>% tbl_df()
# Format comments function ---------------------------------------------
format_comment <- function(text){
  text %>% 
    tolower %>% 
    str_replace_all("([""�������������???''~])", " \\1 ") %>% 
    str_replace_all("([[:punct:]])", " \\1 ") %>% 
    str_replace_all("\\xa0", '') %>% 
    str_replace_all("\\n", ' ') %>% 
    stri_escape_unicode() %>% 
    gsub("(\\w)\\1+(*SKIP)(*F)|(\\w+?)\\2+", " \\2 ", ., perl=TRUE) # transform repeated words
}
train <- train %>%mutate(comment_text = format_comment(comment_text))
test <- test %>%mutate(comment_text = format_comment(comment_text))
#Taking Sample from training data as test data for accuracy purpose---------------------------------------
pseudo_test <- train[c(1:70000),]
pseudo_train <- train[c(70001:nrow(train)),]
# Vectorize comments -------------------------------------------------------------------------------------
# Pseudo Train data
it_pseudo_train <- itoken(pseudo_train$comment_text, 
                   tokenizer = space_tokenizer, 
                   ids = pseudo_train$id, 
                   progressbar = TRUE)
vocab <-  create_vocabulary(it_pseudo_train, ngram = c(1, 2))
pruned_vocab <- prune_vocabulary(vocab,
                                 term_count_min = 10,
                                 doc_proportion_max = 0.8)
vectorizer = vocab_vectorizer(pruned_vocab)
# TF-IDF and Document Term Matrix
tfidf <- TfIdf$new(smooth_idf = TRUE, norm = "l2", sublinear_tf = TRUE)
dtm_train_tfidf <- create_dtm(it_pseudo_train, pruned_vocab) %>% 
fit_transform(tfidf)
# Test data
it_pseudo_test = itoken(pseudo_test$comment_text, 
                 tokenizer = space_tokenizer, 
                 ids = pseudo_test$id, 
                 progressbar = TRUE)
dtm_test_tfidf <- create_dtm(it_pseudo_test, pruned_vocab) %>% 
  transform(tfidf)
x <- dtm_train_tfidf
test_x <- dtm_test_tfidf
rm(dtm_train_tfidf, dtm_test_tfidf); gc();
# Naive bayes features --------------------------------------------------------------------------------
pr <- function(y_i, y, x){
  p <- Matrix::colSums(x[y == y_i, ])
  (p + 1)/(sum(y == y_i) + 1)
}
# Convert dcGtmarix to SparseM format (require by liblinear) ------------------------------------------
convert_sparse_matrix <- function(X){
  X.csc <- new("matrix.csc", ra = X@x,
               ja = X@i + 1L,
               ia = X@p + 1L,
               dimension = X@Dim)
  X.csr <- as.matrix.csr(X.csc)
  X.csr
}
# Fit logistic regression with NB features ------------------------------------------------------------
fit_nblr <- function(y = "toxic", cost = 1/20){
  y <- pseudo_train[[y]]
  r <- log(pr(1, y, x)/pr(0, y, x))
  x_nb <- x %*% Diagonal(x = r)
  x_nb <- convert_sparse_matrix(x_nb)
  fit <- LiblineaR(data = x_nb,
                   target = y,
                   type = 7,
                   cost = cost,
                   epsilon = 1e-4,
                   bias = 1)
  x_test <- test_x %*% Diagonal(x = r)
  x_test <- convert_sparse_matrix(x_test)
  pred <- predict(fit, x_test, proba = TRUE, decisionValues = TRUE)$probabilities[,"1"]
  return(pred)
}
# Generate Output ---------------------------------------------------------------------------------------
subm <- data.frame(id = pseudo_test$id)
costs <- c(0.12, 0.08, 0.08, 0.08, 0.08, 0.08)
category <- c("toxic", "severe_toxic", "obscene", "threat", 
              "insult", "identity_hate")
for(i in 1:length(category)){
  print(category[i])
  subm[, category[i]] <- fit_nblr(category[i], costs[i])
}
subm$toxic <- round(subm$toxic)
subm$severe_toxic <- round(subm$severe_toxic)
subm$obscene <- round(subm$obscene)
subm$threat <- round(subm$threat)
subm$insult <- round(subm$insult)
subm$identity_hate <- round(subm$identity_hate)
# Checking Accuracy--------------------------------------------------------------------------------------
toxic_check <- table(pseudo_test$toxic , subm$toxic)
sum(diag(toxic_check))/sum(toxic_check)
severe_check <- table(pseudo_test$severe_toxic ,subm$severe_toxic)
sum(diag(severe_check))/sum(severe_check)
obscene_check <- table(pseudo_test$obscene ,subm$obscene)
sum(diag(obscene_check))/sum(obscene_check)
threat_check <- table(pseudo_test$threat, subm$threat)
sum(diag(threat_check))/sum(threat_check)
insult_check <- table(pseudo_test$insult , subm$insult)
sum(diag(insult_check))/sum(insult_check)
identity_check <- table(pseudo_test$identity_hate ,subm$identity_hate)
sum(diag(identity_check))/sum(identity_check)
# Working on Main test data-------------------------------------------------------------------------------
# Vectorize comments -------------------------------------------------------------------------------------
# Train data
it_train <- itoken(train$comment_text, 
                          tokenizer = space_tokenizer, 
                          ids = train$id, 
                          progressbar = TRUE)
vocab <-  create_vocabulary(it_train, ngram = c(1, 2))
pruned_vocab <- prune_vocabulary(vocab,
                                 term_count_min = 10,
                                 doc_proportion_max = 0.8)
vectorizer = vocab_vectorizer(pruned_vocab)
# TF-IDF and Document Term Matrix
tfidf <- TfIdf$new(smooth_idf = TRUE, norm = "l2", sublinear_tf = TRUE)
dtm_train_tfidf <- create_dtm(it_train, pruned_vocab) %>% 
  fit_transform(tfidf)
# Test data
it_test = itoken(test$comment_text, 
                        tokenizer = space_tokenizer, 
                        ids = test$id, 
                        progressbar = TRUE)
dtm_test_tfidf <- create_dtm(it_test, pruned_vocab) %>% 
  transform(tfidf)
x <- dtm_train_tfidf
test_x <- dtm_test_tfidf
rm(dtm_train_tfidf, dtm_test_tfidf); gc();
# Naive bayes features --------------------------------------------------------------------------------
pr <- function(y_i, y, x){
  p <- Matrix::colSums(x[y == y_i, ])
  (p + 1)/(sum(y == y_i) + 1)
}
# Convert dcGtmarix to SparseM format (require by liblinear) ------------------------------------------
convert_sparse_matrix <- function(X){
  X.csc <- new("matrix.csc", ra = X@x,
               ja = X@i + 1L,
               ia = X@p + 1L,
               dimension = X@Dim)
  X.csr <- as.matrix.csr(X.csc)
  X.csr
}
# Fit logistic regression with NB features ------------------------------------------------------------
fit_nblr <- function(y = "toxic", cost = 1/20){
  y <- train[[y]]
  r <- log(pr(1, y, x)/pr(0, y, x))
  x_nb <- x %*% Diagonal(x = r)
  x_nb <- convert_sparse_matrix(x_nb)
  
  fit <- LiblineaR(data = x_nb,
                   target = y,
                   type = 7,
                   cost = cost,
                   epsilon = 1e-4,
                   bias = 1)
  
  x_test <- test_x %*% Diagonal(x = r)
  x_test <- convert_sparse_matrix(x_test)
  pred <- predict(fit, x_test, proba = TRUE, decisionValues = TRUE)$probabilities[,"1"]
  return(pred)
}
# Generate Output ---------------------------------------------------------------------------------------
subm <- data.frame(id = test$id)
costs <- c(0.12, 0.08, 0.08, 0.08, 0.08, 0.08)
category <- c("toxic", "severe_toxic", "obscene", "threat", 
              "insult", "identity_hate")
for(i in 1:length(category)){
  print(category[i])
  subm[, category[i]] <- fit_nblr(category[i], costs[i])
}
subm$toxic <- round(subm$toxic)
subm$severe_toxic <- round(subm$severe_toxic)
subm$obscene <- round(subm$obscene)
subm$threat <- round(subm$threat)
subm$insult <- round(subm$insult)
subm$identity_hate <- round(subm$identity_hate)
write.csv(subm, 'submission2.csv', row.names = FALSE)
